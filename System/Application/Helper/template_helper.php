<?php if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }

	function embed($type, $which) {
		if ( is_file(APPLICATION_PATH . ucfirst($type) . 's/' . $which . '_' . $type . '.php' ) ) {
			include APPLICATION_PATH . ucfirst($type) . 's/' . $which . '_' . $type . '.php';
		}
	}

	function icon($name, $style=null) {
		if ( is_file(APPLICATION_PATH . 'Assets/Icons/' . $name . '.png' ) ) {
			return '<img src="'.ASSET_URL . 'Icons/' . $name . '.png" style="'.$style.'" />';
		}
	}