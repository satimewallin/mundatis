<?php
namespace System\Application\Library;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }   

class Config extends Mundatis
{
    function __construct() {
        parent::__construct();
        if ( !is_file(CONFIGURATION_PATH . 'Mundatis_config.ini') ) {
            Mundatis::kill_application('Could not find Mundatis_config.ini');
        }
    }

    public function whoami() {
        return __METHOD__;
    }

    static public function get_configuration_parameter($parameter=NULL) {
    	$main_configuration_file = CONFIGURATION_PATH . 'Mundatis_config.ini';
    	$parsed_ini_file = parse_ini_file($main_configuration_file, true);
    	if ( $parameter == NULL ) {
    		return $parsed_ini_file;
    	}
    	return $parsed_ini_file[$parameter];
    }
}
