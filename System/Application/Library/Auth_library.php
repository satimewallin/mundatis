<?php
namespace System\Application\Library;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }   

class Auth extends Mundatis
{
    function __construct() {
        parent::__construct();
        self::save_new_user_to_database();
    }

    public function whoami() {
        return __METHOD__;
    }

    private function save_new_group_to_database($group_name=null) {
    	if ( $group_name == null ) { return false; }
    	Db::save_data(
    			'auth_groups',
    			array('group_name' => $group_name)
    		);
    }

    private function generate_passphrase_string($user_name, $user_password) {
    	$security_parameters = Config::get_configuration_parameter('security');
    	if ( strlen($user_name) > 10 ) {
    		$len = strlen($user_name);
    	} else {
    		$len = strlen($user_name) * 2;
    	}
    	return substr(hash('haval256,5', $user_name . $security_parameters['salt'] . $user_password), $len, $len * 2);
    }

    private function save_new_user_to_database($raw_user_array=null) {
    	if ( $raw_user_array == null || count($raw_user_array) == 0 ) { return false; }
		$user_array = array(
    			'user_name' => $raw_user_array['user_name'],
    			'user_email' => $raw_user_array['user_email'],
    			'user_passphrase' => self::generate_passphrase_string($user_name, $user_password),
    			'group_id' => $raw_user_array['group_id']
    		);
    	Db::save_data( 'auth_users', $user_array );
    }
}