<?php
namespace System\Application\Library;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }   

class Url extends Mundatis
{
    function __construct() {
        parent::__construct();
        $get_array = $this->_split_get_array($this->_make_get_array_safe($_GET));
        $this->parse_get_variables($get_array);
    }

    public function debug_return_get_variables() {
        $get_array = self::_split_get_array(self::_make_get_array_safe($_GET));
        if ( self::_check_for_get_variables($get_array) == true ) {
            return $get_array;
        }
        return array('No GET-variables sent');
    }

    private function _check_for_get_variables($get_array) {
        if (    is_array($get_array) && 
                count($get_array) != 0 && 
                strlen($get_array[0]) > 3 ) {
            return true;
        }
        return false;
    }

    public function parse_get_variables($get_array=array()) {
        $this->return_package_controller('error');
    	if ( $this->_check_for_get_variables($get_array) == true) {
            $restructured_get_array = $this->_restructure_get_array($get_array);
            if ( $this->_check_for_package($restructured_get_array['package']) == true ) {
                $this->return_package_controller($restructured_get_array['package']);
                $package = ucfirst($restructured_get_array['package']) . '_controller';
                if (    array_key_exists('method', $restructured_get_array) == FALSE ) {
                    call_user_func("\\System\\Application\\Package\\". $package . '::index');
                } else {
                    $class = '\\System\\Application\\Package\\' . $package;
                    $method = $restructured_get_array['method'];
                    if ( method_exists($class, $method) ) {
                        call_user_func($class . "::". $method);
                    }else{
                        \System\Application\Package\Error_controller::not_found();
                    }
                }
            } else {
                $this->return_package_controller('error');
                if ( !array_key_exists('method', $restructured_get_array) || $restructured_get_array['method'] == NULL ) {
                    \System\Application\Package\Error_controller::index();
                } else {
                    $class = '\\System\\Application\\Package\\Error_controller';
                    $method = $restructured_get_array['method'];
                    if ( method_exists($class, $method) ) {
                        \System\Application\Package\Error_controller::{$method}();
                    }else{
                        \System\Application\Package\Error_controller::not_found();
                    }
                }
            }
    	} else {
            $this->return_package_controller('frontpage');
            \System\Application\Package\Frontpage_controller::index();
        }
    }

    private function _check_for_package($package=null) {
        if ( is_dir(PACKAGE_PATH . ucfirst($package) . '_package') &&
             is_file(PACKAGE_PATH . ucfirst($package) . '_package/Library/' . ucfirst($package) . '_controller.php')) {
            return true;
        }
        return false;
    }

    public function return_package_controller($package=null) {
        $declared_classes = get_declared_classes();
        if ( !is_file(PACKAGE_PATH . ucfirst($package) . '_package/Library/' . ucfirst($package) . '_controller.php') )
        {
            Mundatis::kill_application('Could not include ' . PACKAGE_PATH . ucfirst($package) . '_package/Library/' . ucfirst($package_name) . '.php');
        }
        $package_name = ucfirst($package) . '_controller';
        require_once(PACKAGE_PATH . ucfirst($package) . '_package/Library/' . ucfirst($package_name) . '.php');
        $library_namespace = 'System\\Application\\Package\\'.$package_name;
        if ( in_array(ltrim($library_namespace, '\\'), $declared_classes) == FALSE ) {
            $this->{$package_name} = new $library_namespace;
        }
    }

    private function _split_get_array($get_array=null) {
        if ( !is_array($get_array) || count($get_array) == 0 || !array_key_exists('p', $get_array) ) { return false; }
        return explode('/', trim($get_array['p'], '/'));
    }

    private function _restructure_get_array($get_array=null) {
        if ( !is_array($get_array) || count($get_array) == 0 ) { return false; }
        $restructured_get_array['package'] = $get_array[0];
        if ( array_key_exists(1, $get_array) ) {
            $restructured_get_array['method'] = $get_array[1];
        }
        if ( count($get_array) > 2 ) {
            unset($get_array[0]);
            unset($get_array[1]);
            $i = 1;
            foreach ( $get_array as $array_key => $array_value ) {
                $restructured_get_array['arg' . $i] = $array_value;
                $i++;
            }
        }
        return $restructured_get_array;
    }

    private function _make_get_array_safe($get_array=null) {
    	if ( !is_array($get_array) || count($get_array) == 0 ) { return false; }
    	foreach ( $get_array as $array_key => $array_value ) {
    		$get_array[$array_key] = self::_clean_array_value($array_value);
    	}
    	return $get_array;
    }

    private function _clean_array_value($array_value=null) {
    	$security_parameters = Config::get_configuration_parameter('security');
    	$replacement_array = str_split($security_parameters['unsafe_characters']);
    	return str_replace($replacement_array, '', $array_value);
    }
}