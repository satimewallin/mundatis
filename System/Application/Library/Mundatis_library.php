<?php
namespace System\Application\Library;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); } 

class Mundatis
{
    function __construct() {
        if (get_class($this) == 'System\Application\Library\Mundatis') {
            self::load_all_found_core_helpers();
            self::load_all_found_core_libraries();
        }
    }

    function load_all_found_core_libraries() {
    	$library_files = self::_get_all_files_from_path(LIBRARY_PATH);
		foreach ( $library_files as $library ) {
            $declared_classes = get_declared_classes();
			$library = str_replace('_library.php', '', $library);
			$class = str_replace('\\', '/', $library) . '_library.php';
			if ( is_file($class) ) {
				require_once($class);
			}elseif ( is_file(LIBRARY_PATH . $class) ) {
				require_once(LIBRARY_PATH . $class);
			}
			$library_name = strtolower($library);
            $library_namespace = 'System\\Application\\Library\\'.$library;
            if ( in_array(ltrim($library_namespace, '\\'), $declared_classes) == FALSE ) {
                $this->{$library_name} = new $library_namespace;
            }
		}
	}

    public function kill_application($msg=null) {
        echo('<div style="background:#AF0000;color:#ffffff;border:1px solid #313131;padding:3em;font-size:120%;width:93%;">
                '.$msg.'
            </div>');
        die();
    }

    function load_all_found_core_helpers() {
        $helper_files = self::_get_all_files_from_path(HELPER_PATH);
        foreach ( $helper_files as $helper ) {
            if ( is_file(HELPER_PATH . $helper) ) {
                require_once(HELPER_PATH . $helper);
            }
        }
    }

    private function _get_all_files_from_path($PATH=NULL) {
        $files = self::_clean_file_list(scandir($PATH));
        return $files;
    }

    private function _clean_file_list($files=array()) {
        $new_file_array = array();
    	$files_that_are_not_needed = array( '.', '..', str_replace(LIBRARY_PATH, '', realpath(__FILE__)) );
    	foreach ( $files as $file ) {
            if ( (strlen($file) <= 4) || (substr($file, 0, 1) == '.') )
                continue;
	    	if ( ($array_key = array_search($file, $files_that_are_not_needed)) === FALSE ) {
	    		$new_file_array[] = $file;
	    	}
    	}
    	return $new_file_array;
    }


    function __destruct() {
        if ( !defined('RUNNING_IN_DEVELOPMENT') ) {
            $misc_configuration_parameters = Config::get_configuration_parameter('misc');
            $database_configuration_parameters = Config::get_configuration_parameter('database');
            if ( $misc_configuration_parameters['state'] == "Development" ) {
                $last_php_error = error_get_last();
                $dir = BASE_PATH;
                $output = array();
                chdir($dir);
                exec("git log",$output);
                $history = array();
                foreach($output as $line) {
                    if ( count($history) < 1 ) {
                        if(strpos($line, 'commit')===0) {
                            if(!empty($commit)) {
                                array_push($history, $commit);  
                                unset($commit);
                            }
                            $commit['hash'] = substr($line, strlen('commit'));
                        } else if (strpos($line, 'Author')===0) {
                            $commit['author'] = substr($line, strlen('Author:'));
                        } else if(strpos($line, 'Date')===0) {
                            $commit['date'] = substr($line, strlen('Date:'));
                        } else {   
                            $commit['message'] .= $line;
                        }
                    }
                }
                $get_array = Url::debug_return_get_variables();
                echo('<div style="width:100%;background:#313131;color:#fbfbfb;padding: 2em;">
                        <strong style="color:#009FF4;">DEBUG (in Development -mode)</strong>
                            <ul style="margin:1em 0 0 2em;list-style-type:none;">
                                <li><strong style="color:#FFBF00;">Host : </strong> '.HOST.'</li>
                                <li><strong style="color:#FFBF00;">GET : </strong> <ol style="margin:0 0 1em 6.2em;">');
                                    foreach ( $get_array as $get_item ) {
                                        echo '<li>'.$get_item.'</li>';
                                    }
                                echo('</ol></li>
                                <li><strong style="color:#FFBF00;">Current Template : </strong> '.CURRENT_TEMPLATE_PATH.'</li>
                                <li><strong style="color:#FFBF00;">DB Settings : </strong> 
                                    mysql : host='.$database_configuration_parameters['host'].' / port='.
                                    $database_configuration_parameters['port'].' / dbname='.
                                    $database_configuration_parameters['database'].' / user=' .
                                    $database_configuration_parameters['user'].' / password=' .
                                    $database_configuration_parameters['password'].'</li>
                                <li><strong style="color:#FFBF00;">Last PHP Error : </strong> 
                                <strong><em>'.$last_php_error['message'] .'</em></strong> 
                                at line <strong><em>'. $last_php_error['line'] .'</em></strong> 
                                in file <strong><em>'.$last_php_error['file'].'</em></strong>.</li>
                                <li><strong style="color:#FFBF00;">Latest GIT commit : </strong> 
                                    <ul style="margin:1em 0 0 5em;list-style-type:none;">
                                        <li><strong style="color:#C704E5;">Hash : </strong> '.$history[0]['hash'].'</li>
                                        <li><strong style="color:#C704E5;">Author : </strong> '.$history[0]['author'].'</li>
                                        <li><strong style="color:#C704E5;">Date : </strong> '.$history[0]['date'].'</li>
                                        <li><strong style="color:#C704E5;">Msg : </strong> '.$history[0]['message'].'</li>
                                    </ul>
                                </li>
                            </ul>
                    </div>');
                define('RUNNING_IN_DEVELOPMENT', true);
            }
        }
    }

}
