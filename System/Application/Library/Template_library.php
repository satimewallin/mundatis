<?php
namespace System\Application\Library;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }   

class Template extends Mundatis
{
    function __construct() {
        parent::__construct();
        $this->_declare_current_template_path();
        $this->_declare_tpl_paths();
    }

    private function _declare_current_template_path() {
        $misc_configuration_parameters = Config::get_configuration_parameter('misc');
    	defined('CURRENT_TEMPLATE_PATH') || define('CURRENT_TEMPLATE_PATH', BASE_PATH . 'Public/Template/' . $misc_configuration_parameters['current_template'] . '/');
    }

    public function include_template_tpl_file() {
    	require_once(CURRENT_TEMPLATE_PATH . 'tpl.php');
    }

    public function load_view($view_file=null, $data=null) {
        if ( $data != null && is_array($data) && count($data) != 0 ) {
            foreach ( $data as $key => $value )
            {
                ${$key} = $value;
            }
        }
    	if ( is_file(CURRENT_TEMPLATE_PATH . $view_file . '.php') ) {
    		require_once(CURRENT_TEMPLATE_PATH . $view_file . '.php');
    	}
    }

    private function _declare_tpl_paths() {
        $misc_configuration_parameters = Config::get_configuration_parameter('misc');
    	defined('JS_PATH') || define('JS_PATH', HOST . 'Public/Asset/' . $misc_configuration_parameters['current_template'] . '/Javascript/');
    	defined('IMG_PATH') || define('IMG_PATH', HOST . 'Public/Asset/' . $misc_configuration_parameters['current_template'] . '/Image/');
    	defined('CSS_PATH') || define('CSS_PATH', HOST . 'Public/Asset/' . $misc_configuration_parameters['current_template'] . '/Stylesheet/');
    }

    function __destruct() { }
}