<?php

/*

 Prepared statements are used for the queries, don't escape variables.



 *** Update an entry ***

 query_dont_care('
    UPDATE  User
    SET     name = $name
    WHERE   user_id = $user_id
 ', array(
    'user_id' => 7,
    'name' => 'Test test',
 ));



 *** Delete an entry ***

 query_dont_care('
    DELETE FROM User
    WHERE   user_id = $user_id
 ', array(
    'user_id' => 3,
 ));



 *** Create an entry and get ID ***

 $user_id = query_get_id('
    INSERT INTO User
    SET     name = $name
 ', array(
    'name' => 'Test Test',
 ));



 *** Get and print one entry ***

 $user = query_first('
    SELECT  name
    FROM    User
    WHERE   user_id = $user_id
 ', array(
    'user_id' => 9,
 ));

 print $user['name'] . "\n";



 *** Get and update one entry ***

 $user = query_first('
    SELECT  user_id,
            name
    FROM    User
    WHERE   user_id = $user_id
 ', array(
    'user_id => 3,
 ));

 $user['name'] =  $user['name'] . ' FOOOOO';

 query_dont_care('
    UPDATE  User
    SET     name = $name
    WHERE   user_id = $user_id
 ', $user);



 *** Get multiple entries ***

 $user_list = query_all('
    SELECT  user_id,
            name
    FROM    User
 ');



 *** Get and print multiple entries ***

 $user_list = query_all('
    SELECT  *
    FROM    User
    ORDER BY name
 ');
 foreach ($user_list as $user) {
    print $user['name'] . "\n";
 }

*/


namespace System\Application\Library;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }   

use PDO;

class Db extends Mundatis
{

    static $db;
    private $_query_data = array();

    function __construct() {
        parent::__construct();
        self::_connect_to_database();
    }

    public function whoami() {
        return __METHOD__;
    }

    private function _connect_to_database() {
        $database_configuration_parameters = Config::get_configuration_parameter('database');
        try{
            self::$db = new PDO('mysql:host='.$database_configuration_parameters['host'].';
                port='.$database_configuration_parameters['port'].';
                dbname='.$database_configuration_parameters['database'],
                $database_configuration_parameters['user'], 
                $database_configuration_parameters['password'], array( 
                        PDO::ATTR_PERSISTENT => false ,
                        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

        } catch (Exception $e) {
            throw new \Exception('Could not connect to the database');
        }
    }

    public function save_data($table_name=null, $insertion_array=null) {
        $fields = array();
        $values = array();
        foreach ( $insertion_array as $insertion_key => $insertion_value ) {
            $fields[] = $insertion_key;
            $values[] = '\'' . $insertion_value . '\'';         
        }
        $fields_string = implode(', ', $fields);
        $values_string = implode(', ', $values);
        $query = self::$db->prepare("INSERT INTO {$table_name} ({$fields_string}) VALUES({$values_string})");
        try {
            $query->execute();
        } catch(\PDOException $ex) {
            echo('Could not execute the prepared SQL Query: ' . $ex->getMessage());
        }
    }

    private function _add_variable($data) {
        $this->_query_data[] = $data;
        return '?';
    }

    public function query($sql = null, $data = null, $fetch_type = null, $debug = false) {
        if ($fetch_type == null)
            throw new \Exception('Invalid call to function');

        $this->_query_data = array();

        $query_string = preg_replace('/\$([a-zA-Z0-9_]+)/e', 'self::_add_variable($data["$1"])', $sql);
        if ($debug) {
            error_log(str_repeat('*', 10) . ' START ' . str_repeat('*', 10));
            error_log($query_string);
            foreach ($this->_query_data as $value)
                error_log($value);
            error_log(str_repeat('*', 10) . '  END  ' . str_repeat('*', 10));
        }

        $query = self::$db->prepare($query_string);

        try {
            // Bind values.
            $bind_index = 1;
            foreach ($this->_query_data as &$value) {
                if (is_int($value))
                    $param = PDO::PARAM_INT;
                elseif (is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif (is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif (is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    throw new \Exception('Invalid variable type for query.');

                if ($query->bindParam($bind_index, $value, $param) === FALSE)
                    throw new \Exception('Bind param failed.');

                $bind_index++;
            }
            unset($value);
        
            if ($query->execute($this->_query_data) !== TRUE)
                throw new \Exception('Query failed.');

            if ($fetch_type == 'all')
                $result = $query->fetchAll();
            elseif ($fetch_type == 'first')
                $result = $query->fetch(PDO::FETCH_ASSOC);
            elseif ($fetch_type == 'nothing')
                $result = TRUE;
            elseif ($fetch_type == 'id')
                $result = self::$db->lastInsertId(); // DEBUG/NOTE/FIXME/TODO: Is this safe with static $db?
            else
                throw new \Exception('Invalid fetch_type.');

            return $result;
        } catch(\PDOException $ex) {
            echo('Could not execute the prepared SQL Query: ' . $ex->getMessage());
        }
    }

    public function query_all($sql = null, $data = array(), $debug = false) {
        return self::query($sql, $data, 'all', $debug);
    }

    public function query_first($sql = null, $data = array(), $debug = false) {
        return self::query($sql, $data, 'first', $debug);
    }

    public function query_dont_care($sql = null, $data = array(), $debug = false) {
        return self::query($sql, $data, 'nothing', $debug);
    }

    public function query_get_id($sql = null, $data = array(), $debug = false) {
        return self::query($sql, $data, 'id', $debug);
    }
}
