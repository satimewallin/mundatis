<?php if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); } ?>


	<dl class="contained tabs">
        <dd class="active"><a href="#contactForm">Login</a></dd>
        <dd><a href="#contactPeople"><?php echo icon('help'); ?></a></dd>
      </dl>

      <ul class="tabs-content contained">
        <li id="contactFormTab" class="active"><form method="POST" action="<?php echo HOST ?>user/login">
          <div class="row collapse">
            <div class="four columns"><label class="inline">Username</label></div>
            <div class="eight columns"><input type="text" id="username" />
          </div>
          <div class="row collapse">
            <div class="four columns"><label class="inline">Password</label></div>
            <div class="eight columns"><input type="password" id="password" />
          </div>

          <button type="submit" class="radius button right">Login</button>
        </form></li>

        <li id="contactPeopleTab">
          <p>
          	This is the login form
          </p>
        </li>
      </ul>