<?php
namespace System\Application\Package;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }

use \System\Application\Library\Mundatis as Mundatis;
use \System\Application\Library\Template as Template;
use \System\Application\Library\Url as Url;

class Frontpage_controller extends Mundatis
{
    function __construct() {
        parent::__construct();
    }

    function index() {
        Template::load_view('tpl');
    }
}