<?php
namespace System\Application\Package;
 if ( !defined('DIRECT_ACCESS') ) { die('Direct access is not allowed!'); }   

use \System\Application\Library\Mundatis as Mundatis;

class Error_controller extends Mundatis
{
    function __construct() {
        parent::__construct();
    }

    function index() {
    	echo 'Error!';
    }

    function asd() {
    	echo 'asd';
    }

    function not_found() {
        Mundatis::kill_application('The URL you are trying to reach does not exist.');
    }
}