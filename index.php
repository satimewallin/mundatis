<?php

	/** Define the HOST
	**/
	defined('HOST')					|| define('HOST', 'http://localhost/');
	defined('ASSET_URL')			|| define('ASSET_URL', HOST . 'System/Application/Assets/');

	/** COPYRIGHT 2012 David V. Wallin <david@dwall.in>
	**	COPYRIGHT 2012 Johan Söderberg <js-mundatis@bitnux.com>
	**	No License decided yet.
	**	Do not re-distribute until a License is included
	**	with the project.
	**/
	
	/** Core path defined
	**/
	defined('BASE_PATH')			|| define('BASE_PATH', dirname(realpath(__FILE__)) . '/');
	defined('APPLICATION_PATH') 	|| define('APPLICATION_PATH', BASE_PATH . 'System/Application/');
	defined('CONFIGURATION_PATH')	|| define('CONFIGURATION_PATH', APPLICATION_PATH . '/Config/');
	defined('PACKAGE_PATH')			|| define('PACKAGE_PATH', BASE_PATH . 'System/Package/');
	defined('LIBRARY_PATH')			|| define('LIBRARY_PATH', APPLICATION_PATH . 'Library/');
	defined('HELPER_PATH')			|| define('HELPER_PATH', APPLICATION_PATH . 'Helper/');

	/** Define direct access
	**/
	defined('DIRECT_ACCESS')	|| define('DIRECT_ACCESS', true);

	/** Include the Bootstrap Library
	**/
	require(LIBRARY_PATH . 'Mundatis_library.php');

	/** Now we use the namespace and declare the Mundatis class
	**/
	use \System\Application\Library\Mundatis as mundatis;
	$mundatis = new Mundatis();