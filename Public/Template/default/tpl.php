<?php include(CURRENT_TEMPLATE_PATH . 'header.php'); ?>
	<?php
		if ( isset($subpage) && $subpage != NULL ) {
			include(CURRENT_TEMPLATE_PATH . 'subpages/' . $subpage . '.php');
		} else {
			include(CURRENT_TEMPLATE_PATH . 'index.php');
		}
	?>

    <div class="four columns">
      <?php include(CURRENT_TEMPLATE_PATH . 'sidebar.php'); ?>
    </div>
  </div>
<?php include(CURRENT_TEMPLATE_PATH . 'footer.php'); ?>