
  
  
  <!-- Included JS Files (Uncompressed) -->
  <!--
  
  <script src="<?php echo JS_PATH; ?>jquery.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.mediaQueryToggle.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.forms.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.reveal.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.orbit.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.navigation.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.buttons.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.tabs.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.tooltips.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.accordion.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.placeholder.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.alerts.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.topbar.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.joyride.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.clearing.js"></script>
  
  <script src="<?php echo JS_PATH; ?>jquery.foundation.magellan.js"></script>
  
  -->
  
  <!-- Included JS Files (Compressed) -->
  <script src="<?php echo JS_PATH; ?>jquery.js"></script>
  <script src="<?php echo JS_PATH; ?>foundation.min.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="<?php echo JS_PATH; ?>app.js"></script>

  
    <script>
    $(window).load(function(){
      $("#featured").orbit();
    });
    </script> 
  
</body>
</html>