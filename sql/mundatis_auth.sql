-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `auth_groups`;
CREATE TABLE `auth_groups` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `auth_groups` (`group_id`, `group_name`) VALUES
(1,	'Administrator');

DROP TABLE IF EXISTS `auth_permissions`;
CREATE TABLE `auth_permissions` (
  `permission_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) unsigned NOT NULL,
  `app_id` int(11) NOT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `auth_permissions_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


DROP TABLE IF EXISTS `auth_users`;
CREATE TABLE `auth_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` text COLLATE utf8_bin NOT NULL,
  `user_email` text COLLATE utf8_bin NOT NULL,
  `user_passphrase` text COLLATE utf8_bin NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `auth_users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


-- 2012-12-03 20:52:08
